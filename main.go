package main

import (
	// "the-way-to-go/arraySlices"
	// "the-way-to-go/mappes"
	"fmt"

	// r "the-way-to-go/readings"
	// e "the-way-to-go/errorHandling"
	g "the-way-to-go/goroutines"
	"the-way-to-go/structPack"
)

// Main Function
func main() {
	// for i := -1; i < 10; i++ {
	// 	result := fibonacci(i)
	// 	fmt.Printf("fibonacci(%d) : %d\n", i, result)
	// }

	// callback(1, Add)

	// func(a, b int) {
	// 	fmt.Printf("%d + %d = %d\n", a, b, a+b)
	// }(1, 2)

	// func() {
	// 	sum := 0
	// 	for i := 1; i < 1e6; i++ {
	// 		sum += i
	// 	}
	// 	fmt.Println("Sum = ", sum)
	// }()

	// Make an Add2 function, give it a name p2, and call it:
	// p2 := Add2()
	// fmt.Printf("Call Add2 for 3 gives: %v\n", p2(3))
	// Make a special Adder function, a gets value 3:
	// TwoAdder := Adder(2)
	// fmt.Printf("The result is: %v\n", TwoAdder(3))

	// Closure Variable Live Through Function
	// The lambda function stores and accumulates the values of its variables
	// var f = Adder2()
	// fmt.Print(f(1), " - ")
	// fmt.Print(f(20), " - ")
	// fmt.Print(f(300))

	// arraySlices.ArrayInt()
	// arraySlices.ArrayInit()
	// arraySlices.ArrayNew()
	// arraySlices.Reslicing()
	// arraySlices.CopyingSlice()
	// arraySlices.ForString()

	// mappes.StandardMaps()
	// mappes.IsPresent()
	// mappes.SliceMap()
	// mappes.SortingMaps()

	// Struct Type
	t := new(structPack.Struct1)

	t.A = 5
	t.B = 7

	x := &structPack.Struct1{1, 2}
	y := structPack.Struct1{5, 7}

	x.A = 11
	x.B = 12

	y.A = 9
	y.B = 10

	fmt.Println("Int a : ", t.A)
	fmt.Println("Int b : ", t.B)
	fmt.Println(t)
	fmt.Println(x)
	fmt.Println(y)

	func(t structPack.Struct1) {
		t.A = 111
		t.B = 222
	}(y)

	fmt.Println(x)
	fmt.Println(y)

	// 1- struct as a value type:
	var pers1 structPack.Person
	pers1.FirstName = "Chris"
	pers1.LastName = "Woodward"
	structPack.UpPerson(&pers1)
	fmt.Printf("The name of the person is %s %s\n", pers1.FirstName, pers1.LastName)

	// 2—struct as a pointer:
	pers2 := new(structPack.Person)
	pers2.FirstName = "Chris"
	pers2.LastName = "Woodward"
	(*pers2).LastName = "Woodward" // this is also valid
	structPack.UpPerson(pers2)
	fmt.Printf("The name of the person is %s %s\n", pers2.FirstName, pers2.LastName)

	// 3—struct as a literal:
	pers3 := &structPack.Person{"Chris", "Woodward"}
	structPack.UpPerson(pers3)
	fmt.Printf("The name of the person is %s %s\n", pers3.FirstName, pers3.LastName)

	tt := structPack.TagType{true, "Barak Obama", 1}
	for i := 0; i < 3; i++ {
		structPack.RefTag(tt, i)
	}

	// Anonymous Field And Embedded Struct
	outer := new(structPack.OuterS)
	outer.B = 6
	outer.C = 7.5
	outer.In1 = 5
	outer.In2 = 10

	fmt.Printf("outer.b is: %d\n", outer.B)
	fmt.Printf("outer.c is: %f\n", outer.C)
	fmt.Printf("outer.in1 is: %d\n", outer.In1)
	fmt.Printf("outer.in2 is: %d\n", outer.In2)

	// with a struct-literal:
	outer2 := structPack.OuterS{6, 7.5, structPack.InnerS{5, 10}}
	fmt.Println("outer2 is: ", outer2)

	fmt.Println("outer2 In2: ", outer2.InnerS.In2)

	// Methods
	two1 := new(structPack.TwoInts)
	two1.A = 12
	two1.B = 10

	fmt.Printf("The sum is: %d\n", two1.AddThem())
	fmt.Printf("Add them to the param: %d\n", two1.AddToParam(20))
	two2 := structPack.TwoInts{3, 4}
	fmt.Printf("The sum is: %d\n", two2.AddThem())

	// Read Write Package
	// r.ReadInput()
	// r.BuffIOReadInput()
	// r.ReadWriteToFile()
	// r.ReadWriteIOUtil()
	// r.FileOutput()
	// r.FileWrite()
	// r.CopyFile("target.txt", "source.txt")
	// r.ReadingArguments()
	// r.EchoFlag()
	// r.ReadBufferFlag()
	// r.IOInterface()
	// r.EncodeDecodeJSON()
	// r.EncodeDecodeXML()
	// r.EncodeDecodeGob()
	// r.WriteGob()
	// r.SHA1Hash()

	// Handling Error
	// e.ErrorNew()
	// e.HandlingError()
	// e.PanicGo()
	// e.PanicRecover()
	// e.PanicPackage()
	// e.EvenOddTest()

	// Goroutines and Channel
	// g.SetGoMaxProcs()
	g.ConcurrentProcess()
}
