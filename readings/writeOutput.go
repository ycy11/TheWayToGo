package readings

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"strings"
)

func FileOutput() {
	outputFile, outputError := os.OpenFile("output.dat", os.O_WRONLY|os.O_CREATE, 0666)
	if outputError != nil {
		fmt.Printf("An error occured with file creation\n")
		return
	}
	defer outputFile.Close()

	outputWriter := bufio.NewWriter(outputFile)
	outputString := "Hello World!!!\n"

	for i := 0; i < 10; i++ {
		outputWriter.WriteString(outputString)
	}
	outputWriter.Flush()
}

func FileWrite() {
	os.Stdout.WriteString("Hello, World!!!\n")
	f, _ := os.OpenFile("Testing", os.O_CREATE|os.O_WRONLY, 0)
	defer f.Close()
	f.WriteString("Hello, World in A File\n")
}

func CopyFile(dstName, srcName string) (written int64, err error) {
	src, err := os.Open(srcName)
	if err != nil {
		return
	}
	defer src.Close()
	dst, err := os.OpenFile(dstName, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return
	}
	defer dst.Close()
	return io.Copy(dst, src)
}

func ReadingArguments() {
	who := "Alice "
	if len(os.Args) > 1 {
		who += strings.Join(os.Args[1:], " ")
	}
	fmt.Println("Good Morning", who)
}

func EchoFlag() {
	var NewLine = flag.Bool("n", false, "print of newline")
	// echo -n flab, of type *bool

	const (
		Space   = " "
		Newline = "\n"
	)

	flag.PrintDefaults()
	flag.Parse()
	var s string = ""
	for i := 0; i < flag.NArg(); i++ {
		if i > 0 {
			s += Space
		}
		s += flag.Arg(i)
	}

	if *NewLine { // -n is parsed, flag become true
		s += Newline
	}
	os.Stdout.WriteString(s)
}

func cat(r *bufio.Reader) {
	for {
		buf, err := r.ReadBytes('\n')
		if err == io.EOF {
			break
		}
		if string(buf) == "exit\r\n" {
			os.Exit(0)
		}
		fmt.Fprintf(os.Stdout, "%s", buf)
	}
	return
}

func catSlice(f *os.File) {
	const NBUF = 512
	var buf [NBUF]byte
	for {
		switch nr, err := f.Read(buf[:]); true {
		case nr < 0:
			fmt.Fprintf(os.Stderr, "cat: error reading: %s\n", err.Error())
			os.Exit(1)
		case nr == 0: // EOF
			return
		case nr > 0:
			if nw, ew := os.Stdout.Write(buf[0:nr]); nw != nr {
				fmt.Fprintf(os.Stderr, "cat: error writing: %s\n", ew.Error())
			}
		}
	}
}

func ReadBufferFlag() {
	flag.Parse()
	if flag.NArg() == 0 {
		cat(bufio.NewReader(os.Stdin))
	}
	for i := 0; i < flag.NArg(); i++ {
		f, err := os.Open(flag.Arg(i))
		if err != nil {
			fmt.Fprintf(os.Stderr, "%s:error reading from %s: %s\n",
				os.Args[0], flag.Arg(i), err.Error())
			continue
		}
		cat(bufio.NewReader(f))
	}
}

func IOInterface() {
	// unbuffered: os.Stdout implements io.Writer
	fmt.Fprintf(os.Stdout, "%s\n", "hello world! - unbuffered")
	// buffered:
	buf := bufio.NewWriter(os.Stdout)
	// and now so does buf:
	fmt.Fprintf(buf, "%s\n", "hello world! - buffered")
	buf.Flush()
}
