package structPack

import (
	"fmt"
	"reflect"
	"strings"
)

type Struct1 struct {
	A int
	B int
}

type Person struct {
	FirstName string
	LastName  string
}

type TagType struct { //  tags
	Field1 bool   "An important answer"
	Field2 string "The name of the thing"
	Field3 int    "How much there are"
}

type InnerS struct {
	In1 int
	In2 int
}

type OuterS struct {
	B      int
	C      float32
	InnerS // anonymous field
}

func UpPerson(p *Person) {
	p.FirstName = strings.ToUpper(p.FirstName)
	p.LastName = strings.ToUpper(p.LastName)
}

func RefTag(tt TagType, ix int) {
	ttType := reflect.TypeOf(tt)
	ixField := ttType.Field(ix)
	fmt.Printf("%v\n", ixField.Tag)
}
